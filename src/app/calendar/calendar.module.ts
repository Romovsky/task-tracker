import { NgModule } from '@angular/core';
import { DragDropModule } from '@angular/cdk/drag-drop'
import { CalendarComponent } from "./calendar.component";
import { CalendarRoutingModule } from "./calendar-routing.module";
import { CommonModule } from "@angular/common";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    CalendarComponent
  ],
  imports: [
    CalendarRoutingModule,
    DragDropModule,
    CommonModule,
    ReactiveFormsModule
  ]
})
export class CalendarModule { }
