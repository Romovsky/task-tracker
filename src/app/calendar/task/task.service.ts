import { Injectable } from '@angular/core';
import {Task, WeekDay} from "./task";

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  private tasks: Task[] = [];

  weekDays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

  constructor() { }

  createTask(task: Task): void {
    this.tasks.push(task);
    localStorage.setItem('tasks', JSON.stringify(this.tasks));
  }

  deleteTask(task: Task): void {
    this.tasks.splice(this.tasks.findIndex((s_task) => task.time === s_task.time), 1);
    localStorage.setItem('tasks', JSON.stringify(this.tasks));
  }

  editTask(oldTask: Task, newTask: Task ): void {
    // this.tasks.map((s_task) => task.time === s_task.time ? task : s_task);
    this.tasks.splice(this.tasks.findIndex((s_task) => oldTask.time === s_task.time), 1);
    this.tasks.push(newTask);
    localStorage.setItem('tasks', JSON.stringify(this.tasks));
  }

  getWeek( date: Date ): WeekDay[] {
    this.getTasks();
    let weekDay = date.getDay();
    let monday = this.moveDate(date, -weekDay);
    return  Array.from(this.weekDays, (day, i) => {
      return {
        name: day,
        date: this.moveDate(monday, i),
        timeline: Array.from({length: 24}, (v, hour) => {
          return {
            hour: hour,
            tasks: this.getHourTasks(this.moveDate(monday, i), hour)
          }
        })
      }})
  }

  private moveDate( day: Date, offset: number ): Date {
    return new Date(day.getFullYear(), day.getMonth(), day.getDate() + offset);
  }

  private getDateHour( date: Date, hour: number = -1 ) {
    if(hour > 0)
      return new Date(date.getFullYear(), date.getMonth(), date.getDate(), hour);
    else
      return new Date(date.getFullYear(), date.getMonth(), date.getDate());
  }

  private isThatHour( day1: Date, day2: Date ): boolean {
    return day1.getFullYear() === day2.getFullYear()
        && day1.getMonth() === day2.getMonth()
        && day1.getDate() === day2.getDate()
        && day1.getHours() === day2.getHours()
  }

  private getHourTasks( date: Date, hour: number ): Task[] {
    return this.tasks.filter((task) => this.isThatHour(task.time, this.getDateHour(date, hour))).sort((time1, time2) => time1 < time2 ? 1 : -1);
  }

  private getTasks(): void {
    let tasksJSON = localStorage.getItem('tasks');
    if(tasksJSON) {
      this.tasks = JSON.parse(tasksJSON);
      this.tasks.map((task) => task.time = new Date(task.time));
    }

    console.log(this.tasks);
  }
}
