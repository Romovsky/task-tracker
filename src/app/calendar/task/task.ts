export interface Task {
  time: Date,
  title: string,
  description: string,
  duration: number
}

export interface WeekDay {
  name: string,
  date: Date,
  timeline: Timeline[]
}

export interface Timeline {
  hour: number,
  tasks: Task[]
}
