import { Component, OnInit } from '@angular/core';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from "@angular/cdk/drag-drop";
import {Task, WeekDay} from "./task/task";
import {TaskService} from "./task/task.service";
import {BehaviorSubject} from "rxjs";
import {FormControl, FormGroup} from "@angular/forms";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  constructor(private taskService: TaskService) {
    this.week = this.taskService.getWeek(new Date());
  }

  week: WeekDay[];
  $openNewForm: BehaviorSubject<string> = new BehaviorSubject<string>('');
  newTaskForm = new FormGroup({
    title: new FormControl(''),
    description: new FormControl(''),
    hour: new FormControl(''),
    duration: new FormControl(''),
  });

  dropTask(event: CdkDragDrop<Task[], Task[], Task>): void {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    }
    this.moveTask(event);
  }

  moveTask(event: CdkDragDrop<Task[], Task[], Task>): void {
    let newTask: Task = {
      title: event.item.data.title,
      duration: event.item.data.duration,
      description: event.item.data.description,
      time: new Date(event.item.data.time.getFullYear(),
        event.item.data.time.getMonth(),
        event.item.data.time.getDate(),
        event.item.data.time.getHours(),
        event.container.data[event.currentIndex === 0 ? 0 : event.currentIndex - 1].
        time.getMinutes() + (event.currentIndex === 0 ? 0 : 15))
    };
    this.taskService.editTask(event.item.data, newTask) ;
    this.week = this.taskService.getWeek(new Date());
  }

  deleteTask(task: Task): void {
    this.taskService.deleteTask(task);
    this.week = this.taskService.getWeek(new Date());
  }

  new (date: Date) {
    this.taskService.createTask({
      time: new Date(date.getFullYear(), date.getMonth(), date.getDate(), this.newTaskForm.get('hour')?.value),
      title: this.newTaskForm.get('title')?.value,
      description: this.newTaskForm.get('description')?.value,
      duration: this.newTaskForm.get('duration')?.value });
    this.week = this.taskService.getWeek(new Date());
  }


  ngOnInit(): void {
    console.log(this.week)
  }

}
